# avalith-cursos-de-python-2022

Repositorio con el objetivo de condensar mis prácticas junto al contenido del [seminario de Python](/Python/Seminario%20de%20Python.md) y el seminario de Django impartidos por Jose Balseca para la gente de [Avalith](https://www.avalith.net/).

## Seminario de Python

Como muchas de las cosas que vemos se puede resumir en una celda de jupyter, en vez de guardar los archivos como .py los guardo como .ipynb para poder usarlos directamente.

- [Clases grabas y subidas a Youtube](https://www.youtube.com/playlist?list=PLJPvCr6dK-ckJy0yEkk9o1Ij3cbJJJbcI)
- [Practicas realizadas por mí](/Python/Practicas/)
- [PDFs con el contenido referenciado en las clases](/Python/pdfs/)
- [Otros archivos adjuntos que fui coleccionando durante el curso](/Python/Adjuntos/)

## Seminario de Django

- [Videos del seminario](Django/Adjuntos/youtube-videos.md)
- [PDFs con el contenido de las clases](Django/pdfs)

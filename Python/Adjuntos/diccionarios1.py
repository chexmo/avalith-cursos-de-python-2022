empty_dict1 = {}
empty_dict2 = dict()

print(empty_dict1)
print(empty_dict2)
rae = {
    'bifronte': 'De dos frentes o dos caras',
    'anarcoide': 'Que tiende al desorden',
    'auto': 'Medio de transporte'
}
population_can = {
    2015: 2_135_209,
    2016: 2_154_924,
    2017: 2_177_048,
    2018: 2_206_901,
    2019: 2_220_270
}

print(rae)
print(population_can)

lista1 = ['a1', 'b2', 'c5']
lista1_dict = dict(lista1)
print(lista1)
print(lista1_dict)

lista2 = [['valor', 1], ['b', 2], ['c', 5], ['d', 6], ['e', 7], ['f', 'letra'], [1980, 12.90]]
lista2_dict = dict(lista2)
print(lista2)
print(lista2_dict)

person = dict(
    name='Juan',
    surname='Perez',
    job='Developer',
    fecha_nacimiento='2001-02-03',
    edad= 21,
    data = [1.2, 3.4, 7.8],
    preferencias = {
        "deporte": "basket",
        "musica": "pop",
        "libro": "Python"
    }
)

print(person)

print(person['name'])
print(f'Apellido: {person["surname"]}')
print(f'Elemento 2 de data: {person["data"][-1]}')
print(f'Gusta del deporte: {person["preferencias"]["deporte"]}')

print(person.get('name'))
print(person.get('title', 'Not available'))

person['title'] = 'Engineer'

print('Imprime person >>>>')
print(person)

person['age_prom'] = 32
print('Imprime person >>>>')
print(person)

person['title'] = 'Lawyer'
print('Imprime person >>>>')
print(person)

if 'title' in person:
    print(f'tiene title: {person["title"]}')
else:
    print('No tiene title')

if 'city' in person:
    print(f'tiene city: {person["city"]}')
else:
    print('No tiene city')

if 'musica' in person["preferencias"]:
    print(f'tiene musica: {person["preferencias"]["musica"]}')
else:
    print('No tiene musica')

claves = person.keys()
print(claves)


print("Valores dict >>>>>>")

for clave in person.keys():
    print(person[clave])


valores = person.values()
print(valores)

for values in person.values():
    print(values)


for clave, valor in person.items():
    print(f'Clave: {clave}     Valor:{valor}')

print(len(person))

person_new_values = {
    'data1': 1,
    'data2': 3,
    'data4': 'Ecuador'
}

dict_result = {**person, **person_new_values}
print(dict_result)

dict_result2 = person | person_new_values
print(dict_result2)

print('Uniendo diccionarios en person')
print(person)
person.update(person_new_values)
print(person)

del(person["data4"])
print("borrado data4")
print(person)

preferncias = person.pop('preferencias')
print(person)
print(preferncias)

#person.clear()
#print(person)
#person = {}
#print(person)

new_person = person
print(new_person)
person['edad'] = 42
print(new_person)

print("copia real >>>>>>>>>")
new_person1 = person.copy()
print(new_person1) 
person['edad'] = 19
print(new_person1) 
print(person)
print(new_person)

palabras = ('uno', 'ana', 'pie', 'test')

palabras_dict_lenght = { palabra: len(palabra) for palabra in palabras if len(palabra) == 3}
print(palabras_dict_lenght)



import typer

app = typer.Typer()


@app.command()
def hello(name: str):
    """Say hello"""
    print(f"Hello {name}")


@app.command()
def bye(name: str):
    """Say goodbye"""
    print(f"Goodbye {name}")


if __name__ == "__main__":
    app()

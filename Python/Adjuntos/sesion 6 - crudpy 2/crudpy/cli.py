import typer

from sqlalchemy import and_, create_engine, or_
from config import DATABASE_URI
from models import Base, Book, Inventory
from sqlalchemy.orm import sessionmaker
from contextlib import contextmanager
from datetime import datetime


@contextmanager
def session_scope():
    session = Session()
    try:
        yield session
        session.commit()
    except Exception:
        session.rollback()
        raise
    finally:
        session.close()


engine = create_engine(DATABASE_URI)

Session = sessionmaker(bind=engine)
app = typer.Typer()


@app.command()
def query1():
    """Queries"""
    with session_scope() as s:
        r = s.query(Book).filter(Book.author == "Avalith").first()
        print(r)


@app.command()
def query2():
    """Queries"""
    with session_scope() as s:
        book = Book(
            title="Aprendiendo Java",
            author="Avalith",
            pages=200,
            published=datetime(2022, 7, 14),
        )
        s.add(book)
        r = s.query(Book).filter(Book.title == "Aprendiendo Java").first()
        print(r)


@app.command()
def query3():
    """Queries"""
    with session_scope() as s:
        book = Book(
            title="Aprendiendo C#",
            author="Avalith",
            pages=100,
            published=datetime(2022, 6, 14),
        )
        s.add(book)
        r = s.query(Book).filter(Book.title == "Aprendiendo C#").first()
        print(r)


@app.command()
def delete_record():
    """Deletes"""
    with session_scope() as s:
        s.query(Book).filter(Book.title == "Aprendiendo C#").delete()


@app.command()
def update_record():
    """Updates"""
    with session_scope() as s:
        s.query(Book).filter(Book.title == "Aprendiendo Java").update({"pages": "100"})


@app.command()
def query4():
    """Queries"""
    with session_scope() as s:
        r = s.query(Book).filter(and_(Book.pages >= 100, Book.pages <= 800)).all()
        print(r)


@app.command()
def query5():
    """Queries"""
    with session_scope() as s:
        r = (
            s.query(Book)
            .filter(
                or_(Book.title == "Aprendiendo Java", Book.title == "Deep Learning")
            )
            .all()
        )
        print(r)


@app.command()
def query6():
    """Queries"""
    with session_scope() as s:
        r = s.query(Book).order_by(Book.pages.asc()).all()
        print(r)


@app.command()
def query7():
    """Queries"""
    with session_scope() as s:
        r = s.query(Book).order_by(Book.pages.desc()).limit(3).all()
        print(r)


@app.command()
def query8():
    """Queries"""
    with session_scope() as s:
        r = (
            s.query(Book)
            .filter(
                Book.published.between(datetime(1989, 1, 1), datetime(2012, 12, 31))
            )
            .all()
        )
        print(r)


@app.command()
def query9():
    """Queries"""
    with session_scope() as s:
        inventory = Inventory(book_id=1, quantity=10)
        s.add(inventory)
        inventory = Inventory(book_id=2, quantity=20)
        s.add(inventory)
        r = s.query(Inventory).all()
        print(r)


@app.command()
def query10():
    """Queries"""
    with session_scope() as s:
        r = (
            s.query(Book.title, Inventory.quantity)
            .join(Book, Inventory.book_id == Book.id)
            .all()
        )
        print(r)


@app.command()
def query11():
    """Queries"""
    with session_scope() as s:
        r = (
            s.query(Book.title, Inventory.quantity)
            .join(Book, Inventory.book_id == Book.id)
            .filter(Inventory.quantity > 10)
            .all()
        )
        print(r)


@app.command()
def query12():
    """Queries"""
    with session_scope() as s:
        r = (
            s.query(Book.title, Inventory.quantity)
            .join(Book, Inventory.book_id == Book.id)
            .all()
        )
        records = []
        for row in r:
            row_dict = {
                "title": row[0],
                "quantity": row[1],
            }
            records.append(row_dict)
        print(records)


@app.command()
def query13():
    """Queries"""
    with session_scope() as s:
        r = (
            s.query(Book.title, Inventory.quantity)
            .join(Book, Inventory.book_id == Book.id)
            .all()
        )
        records = []
        for row in r:
            row_dict = {
                "title": row.title,
                "quantity": row.quantity,
            }
            records.append(row_dict)
        print(records)


if __name__ == "__main__":
    app()

from fastapi import FastAPI

from sqlalchemy import and_, create_engine, or_
from config import DATABASE_URI
from models import Base, Book, Inventory
from sqlalchemy.orm import sessionmaker
from contextlib import contextmanager
from datetime import datetime
from pydantic import BaseModel


class BookModel(BaseModel):
    title: str
    author: str
    pages: int
    published: str


@contextmanager
def session_scope():
    session = Session()
    try:
        yield session
        session.commit()
    except Exception:
        session.rollback()
        raise
    finally:
        session.close()


app = FastAPI()
engine = create_engine(DATABASE_URI)

Session = sessionmaker(bind=engine)


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/books")
async def get_books():
    with session_scope() as s:
        r = (
            s.query(Book.title, Inventory.quantity)
            .join(Book, Inventory.book_id == Book.id)
            .all()
        )
        records = []
        for row in r:
            row_dict = {
                "title": row.title,
                "quantity": row.quantity,
            }
            records.append(row_dict)
        return records


@app.get("/books/{id}")
async def get_book(id: int):
    with session_scope() as s:
        r = s.query(Book).filter(Book.id == id).first()
        record = {
            "title": r.title,
            "author": r.author,
            "pages": r.pages,
            "published": r.published,
        }
        return record


@app.post("/books")
async def create_book(book: BookModel):
    with session_scope() as s:
        book = Book(
            title=book.title,
            author=book.author,
            pages=book.pages,
            published=book.published,
        )
        s.add(book)

        return {"message": "Book created"}

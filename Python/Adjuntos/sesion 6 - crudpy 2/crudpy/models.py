from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import ForeignKey, Column, Integer, String, Date
from sqlalchemy.orm import relationship

Base = declarative_base()


class Book(Base):  # pylint: disable=too-few-public-methods
    __tablename__ = "books"
    id = Column(Integer, primary_key=True)
    title = Column(String)
    author = Column(String)
    pages = Column(Integer)
    published = Column(Date)

    def __repr__(self):
        return f"title='{self.title}', author='{self.author}', pages={self.pages}, published={self.published}\n"


class Inventory(Base):  # pylint: disable=too-few-public-methods
    __tablename__ = "inventory"
    id = Column(Integer, primary_key=True)
    book_id = Column(Integer, ForeignKey("books.id"))
    quantity = Column(Integer)

    book = relationship("Book")

    def __repr__(self):
        return f"book_id={self.book_id}, quantity={self.quantity}\n"
